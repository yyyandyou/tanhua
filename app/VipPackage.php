<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VipPackage extends Model
{
    //use Notifiable;
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'duration'
    ];
}

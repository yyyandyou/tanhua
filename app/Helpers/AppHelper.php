<?php

namespace App\Helpers;
use App\Library\JinFu;
use App\Library\WechatPay;
use App\Library\AliPay;
use Illuminate\Routing\UrlGenerator;

class AppHelper
{
    public function output($data = '')
    {
        header('Content-Type: text/plain; charset=utf-8');
        if (strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
            ob_start('ob_gzhandler');
        } else {
            ob_start();
        }
        echo json_encode($data);
        ob_end_flush();
        exit;
    }

    public function generateRandom($length = 8, $number_only = false){
        if($number_only){
            $characters = '0123456789';
        }else {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //验证手机号码
    public  static function checkMobile($str)
    {
        if (empty($str)) {
            return false;
        }

        return preg_match('#^13[\d]{9}$|14^[0-9]\d{8}|^15[0-9]\d{8}$|^18[0-9]\d{8}|^17[0-9]\d{8}$|^19[\d]{9}$#', $str);
    }

    public function http_get($url, $data_type='text')
    {
        $cl = curl_init();
        if(stripos($url, 'https://') !== FALSE) {
            curl_setopt($cl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($cl, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($cl, CURLOPT_SSLVERSION, 1);
        }
        curl_setopt($cl, CURLOPT_URL, $url);
        curl_setopt($cl, CURLOPT_RETURNTRANSFER, 1 );
        $content = curl_exec($cl);
        $status = curl_getinfo($cl);
        curl_close($cl);
        if (isset($status['http_code']) && $status['http_code'] == 200) {
            if ($data_type == 'json') {
                $content = (array)json_decode($content);
            }
            return $content;
        } else {
            return FALSE;
        }
    }

    public function allPaymentModes(){
        $payment_modes = array('wxpay', 'alipay', 'alipay_h5');
        return $payment_modes;
    }

    public function buildOrderNo($user_id = 0)
    {
        return date('Ymd').$user_id.mt_rand(100000,999999);
    }

    public function getImageRoot()
    {
        return  url('/');
    }

    public function trimStr($str = '', $type = ''){
        $str = str_replace('"', '', $str);
        $str = str_replace("'", "", $str);
        $str = trim($str);

        if($type == 'int'){
            $str = intval($str);
        }
        return $str;
    }

    // 获取客户端IP地址
    public function get_client_ip(){
        if(getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'),'unknown')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif(getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'),'unknown')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif(getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'),'unknown')) {
            $ip = getenv('REMOTE_ADDR');
        } elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return preg_match ( '/[\d\.]{7,15}/', $ip, $matches ) ? $matches [0] : '';
    }

    function http_post_request($url, $param = NULL, $header = NULL) {
        if(is_array($param)){
            $param = http_build_query($param);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        if(is_array($header)) curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);

        $res = curl_exec( $ch );
        curl_close( $ch );
        return $res;
    }

    public function processPayment($paymentMode = 'alipay_h5', $userId = '', $data = array())
    {
        $status = 200;
        $message = '';
        $return = array();
        $url = '';

        if($userId == ''){
            $status = 400;
            $message = __('message.not_register_yet');
        }else {
            //调用对应支付方法
            switch ($paymentMode) {
                case 'wxpay':
                    $wechatPay = new WechatPay();
                    $return = $wechatPay->pay($data, $userId);

                    if (!$return) {
                        $status = 400;
                        $message = __('message.invalid_payment_account');
                    }
                    break;
                case 'alipay':
                    $aliPay = new AliPay();
                    $return = $aliPay->pay($data, $userId);
                    if (!$return) {
                        $status = 400;
                        $message = __('message.invalid_payment_account');
                    }
                    break;
                case 'alipay_h5':
                    $url = $this->getImageRoot()."/h5pay/".$data['order_id']."/".md5($data['order_id'].number_format($data['amount'],2).$data['order_time']);
                    break;
                default:
                    $status = 400;
                    $message = __('message.create_order_fail');
                    break;
            }
        }
        return array('status' => $status, 'message' =>$message, 'data' => $return, 'url' => $url);
    }

    public static function instance()
    {
        return new AppHelper();
    }
}

<?php

namespace App\Library;

use Illuminate\Database\Eloquent\Model;

class JinFu
{
    private $alipay_account;
    private $jinfu = '';//'jmjf382';
    private $mchid = '';//'98267357';
    public $key = '';//'c8iu30xh91q98p75f2u9f5joccjmijye';
    private $status = 2;
    private $accountInfo = array();
    private $notify_url = URL_PATH . '/api/jinfu_notify';
    //private $notify_url = 'http://testapiv1.gzliantu020.com/api/grpay_grqrcode';
    //private $notify_url = 'http://pay.yousheng666.cn';

    public function __construct(){
        $res = \AppHelper::instance()->http_get("https://api.yueliao88.com/api/jinfu_account");
        $res=json_decode($res,true);

        if(!empty($res)) {
            $this->accountInfo = $res;
            $this->jinfu = $res['jinfu'];
            $this->mchid = $res['mchid'];
            $this->key = $res['key'];
            $this->status = $res['status'];
        }
    }

    private function setAlipayAcccount($data) {
//        $accountInfo['account_name'] =$this->jinfu;
//        $accountInfo['app_channel'] = '';
//        $accountInfo['mch_id']=$this->mchid;
//        $accountInfo['key'] = $this->key;
//        //model('transaction')->update(['order_id' => $data['order_id']],['key' => $accountInfo['key'],'pay_account' => $accountInfo['mch_id']]);

        //if(!empty($res)) {
            $accountInfo = $this->accountInfo;
            $accountInfo['account_name'] = $this->jinfu;
            $accountInfo['app_channel'] = '';
            $accountInfo['mch_id'] = $this->mchid;
        //}

        //update transaction

        return $accountInfo;
    }

    public function getAlipayAccountInfoByMchid($mch_id)
    {
        $accountInfo = false;
        $jinfupasslist = cpConfig::get('jinfupass');
        foreach ($jinfupasslist as $val) {
            if ($val['mch_id'] == $mch_id) {
                $accountInfo = $val;
                break;
            }
        }
        return $accountInfo;
    }

    public function getAlipayAccountInfoByName($account_name)
    {
        $jinfupasslist = cpConfig::get('jinfupass');
        $accountInfo = $jinfupasslist[$account_name];
        return $accountInfo;
    }

    public function alipay($data)
    {
        // 设置金福账号信息
        $accountInfo = $this->setAlipayAcccount($data);
        $mch_id = $accountInfo['mch_id'];	//	商户号
        $key = $accountInfo['key']; // key
        $notify_url = $this->notify_url;
        $return_url = ""; // 同步返回地址

        $version = "1.0";	//  版本号
        $pay_type = 205;	//	通道类型-支付宝H5
        $fee_type = "CNY";	//	货币类型

        $total_amount = $data["amount"] * 100;	//	订单金额
        $device_info = $data['platform'];	//	设备号
        $out_trade_no = $data['order_id'];	//	商户订单号
        $body = $data['name'];	//	商品描述
        $sp_client_ip = \AppHelper::instance()->get_client_ip();    // 终端用户IP
        // $attach = '';    //商户附加信息，可做扩展参数
        $time_start = date("YmdHis");        // 订单生成时间，格式为yyyyMMddHHmmss 注：订单生成时间与失效时间需要同时传入才会生效。
        $time_expire = date('YmdHis', strtotime('+10 minute'));       // 订单失效时间，格式为yyyyMMddHHmmss 注：订单生成时间与失效时间需要同时传入才会生效。
        // $limit_credit_pay = 0;   // 1，禁用信用卡；0，不禁用，默认为0；不支持QQ、JD产品


        $signSource = sprintf("version={$version}&mch_id={$mch_id}&pay_type={$pay_type}&total_amount={$total_amount}&out_trade_no={$out_trade_no}&notify_url={$notify_url}&key={$key}");
        $sign = md5($signSource);

        $attachStr = '';

        $data = [
            "version" => $version,
            "mch_id" => $mch_id,
            "pay_type" => $pay_type,
            "fee_type" => $fee_type,
            "total_amount" => $total_amount,
            "device_info" => $device_info,
            "out_trade_no" => $out_trade_no,
            "notify_url" => $notify_url,
            "body" => $body,
            "attach" => $attachStr,
            "sp_client_ip" => $sp_client_ip,
            "time_start" => $time_start,
            "time_expire" => $time_expire,
            "sign" => $sign,
        ];
        return $data;
    }

    public function getAlipayUrl() {
        return 'https://pay.jinfupass.com/gateway/h5pay';
    }

    /**
     * 金福小程序支付
     * @param array $data
     */
    public function miniAppPay($data) {
        //获取配置参数
        $accountInfo['mch_id'] = '98983512';
        $accountInfo['secret_key'] = 'dbq9ayfujifecsra206b1gp6ryikj51z';
        $accountInfo['notify_url'] = 'https://apiv3.banliao8.com/api/notify_jinfu';
        //model('transaction')->update(['order_id' => $data['order_id']], ['pay_account' => $configs['pay_account']]);
        //组装提交参数
        $time_start = date("YmdHis");        // 订单生成时间，格式为yyyyMMddHHmmss 注：订单生成时间与失效时间需要同时传入才会生效。
        $time_expire = date('YmdHis', strtotime('+10 minute'));       // 订单失效时间，格式为yyyyMMddHHmmss 注：订单生成时间与失效时间需要同时传入才会生效。
        $postData = [
            'version' => '1.0',
            'mch_id' => $accountInfo['mch_id'],
            'pay_type' => '502',
            'fee_type' => 'CNY',
            'total_amount' => strval($data['amount'] * 100),
            'out_trade_no' => $data['order_id'],
            'device_info' => strval($data['platform']),
            'notify_url' => $accountInfo['notify_url'],
            'body' => $data['name'],
            'sp_client_ip' => get_client_ip(),
            'time_start' => $time_start,
            'time_expire' => $time_expire,

        ];
        //计算签名
        $md5Arr = ['version', 'mch_id', 'pay_type', 'total_amount', 'out_trade_no', 'notify_url'];
        $md5Str = '';
        foreach ($md5Arr as $v) {
            $md5Str .= $v . '=' . $postData[$v] . '&';
        }
        $md5Str .= 'key=' . $accountInfo['secret_key'];
        $sign = strtolower(md5($md5Str));
        $postData['sign'] = $sign;

        $re = Http::doPost("https://pay.jinfupass.com/gateway/apppay", $postData, 10);
        $re = json_decode($re, true);
        if (is_array($re) && isset($re['result_code'])) {
            if ($re['result_code'] == 1 && !empty($re['sys_trade_no'])) {
                cpError::log('create jinfu wxmini success: sys_trade_no=' . $re['sys_trade_no'], 'jinfu_log');
                return json_encode([
                    'sys_trade_no' => $re['sys_trade_no'],
                    'app_id' => 'wxb187354d6602ee90',
                    'app_username' => 'gh_316fea1f9a5b',
                ]);
            }
            cpError::log('create jinfu wxmini fail: ' . ($re['return_msg'] ?? ''), 'jinfu_log');
        } else {
            cpError::log('create jinfu wxmini fail', 'jinfu_log');
        }
        return false;
    }
}

<?php
/**
 * 微信支付帮助库
 * ====================================================
 * 接口分三种类型：
 * 【请求型接口】--Wxpay_client_
 * 		统一支付接口类--UnifiedOrder
 * 【响应型接口】--Wxpay_server_
 * 		通用通知接口--Notify
 * =====================================================
 * 【CommonUtil】常用工具：
 * 		trimString()，设置参数时需要用到的字符处理函数
 * 		createNoncestr()，产生随机字符串，不长于32位
 * 		formatBizQueryParaMap(),格式化参数，签名过程需要用到
 * 		getSign(),生成签名
 * 		arrayToXml(),array转xml
 * 		xmlToArray(),xml转 arrayW
*/
namespace App\Library\Payment\Wxpay;
/**
 * 所有接口的基类
 */
class Common_util_pub
{

	function trimString($value)
	{
		$ret = null;
		if (null != $value)
		{
			$ret = $value;
			if (strlen($ret) == 0)
			{
				$ret = null;
			}
		}
		return (string)$ret;
	}

	/**
	 * 	作用：产生随机字符串，不长于32位
	 */
	public function createNoncestr( $length = 32 )
	{
		$chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$str ='';
		for ( $i = 0; $i < $length; $i++ )  {
			$str.= substr($chars, mt_rand(0, strlen($chars)-1), 1);
		}
		return $str;
	}

	/**
	 * 	作用：格式化参数，签名过程需要使用
	 */
	function formatBizQueryParaMap($paraMap, $urlencode)
	{
		$buff = '';
		ksort($paraMap);
		foreach ($paraMap as $k => $v)
		{
		    if($urlencode)
		    {
			   $v = urlencode($v);
			}
			//$buff .= strtolower($k) . '=' . $v . '&';
			$buff .= $k . '=' . $v . '&';
		}
		$reqPar = '';
		if (strlen($buff) > 0)
		{
			$reqPar = substr($buff, 0, strlen($buff)-1);
		}
		return $reqPar;
	}

	/**
	 * 	作用：生成签名
	 */
	public function getSign($Obj,$key)
	{
		foreach ($Obj as $k => $v)
		{
			$Parameters[$k] = $v;
		}
		//签名步骤一：按字典序排序参数
		ksort($Parameters);
		$String = $this->formatBizQueryParaMap($Parameters, false);
		//echo '【string1】'.$String.'</br>';
		//签名步骤二：在string后加入KEY
		$String = $String.'&key='.$key;
		//echo '【string2】'.$String.'</br>';
		//签名步骤三：MD5加密
		$String = md5($String);
		//echo '【string3】 '.$String.'</br>';
		//签名步骤四：所有字符转为大写
		$result_ = strtoupper($String);
		//echo '【result】 '.$result_.'</br>';
		return $result_;
	}

	/**
	 * 	作用：array转xml
	 */
	function arrayToXml($arr)
    {
        $xml = '<xml>';
        foreach ($arr as $key=>$val)
        {
			if (is_numeric($val))
			{
				$xml.='<'.$key.'>'.$val.'</'.$key.'>';
			}
			else{
				$xml.='<'.$key.'><![CDATA['.$val.']]></'.$key.'>';
			}
        }
        $xml.='</xml>';
        return $xml;
    }

	/**
	 * 	作用：将xml转为array
	 */
	public function xmlToArray($xml)
	{
        //将XML转为array
        $array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
		return $array_data;
	}
}

?>

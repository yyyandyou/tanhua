<?php
/**
 * 微信支付帮助库
 * ====================================================
 * 接口分三种类型：
 * 【请求型接口】--Wxpay_client_
 * 		统一支付接口类--UnifiedOrder
 * 【响应型接口】--Wxpay_server_
 * 		通用通知接口--Notify
 * =====================================================
 * 【CommonUtil】常用工具：
 * 		trimString()，设置参数时需要用到的字符处理函数
 * 		createNoncestr()，产生随机字符串，不长于32位
 * 		formatBizQueryParaMap(),格式化参数，签名过程需要用到
 * 		getSign(),生成签名
 * 		arrayToXml(),array转xml
 * 		xmlToArray(),xml转 arrayW
*/

/**
 * 所有接口的基类
 */

/**
 * 请求型接口的基类
 */

namespace App\Library\Payment\Wxpay;
use App\Library\Payment\Wxpay\Wxpay_client_pub;
/**
 * 统一支付接口类
 */
class UnifiedOrder_pub extends Wxpay_client_pub
{
	function __construct()
	{
		//设置接口链接
		$this->url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
	}

	/**
	 * 生成接口参数xml
	 */
	function createXml($key)
	{
	    $this->parameters['nonce_str'] = $this->createNoncestr();
	    $this->parameters['sign'] = $this->getSign($this->parameters, $key);
	    return  $this->arrayToXml($this->parameters);
	}

	/**
	 * 获取prepay_id
	 */
	function getPrepayId($key)
	{
		$this->postXml($key);
		$this->result = $this->xmlToArray($this->response);
		if($this->result['return_code'] == 'SUCCESS')
		{
			return $this->result['prepay_id'];
		}
		else
		{
			exit(json_encode(array('status'=>400, 'msg'=>$this->result['return_msg'])));
		}
	}

	/**
	 * 获取返回结果
	 */
	function getResult($key)
	{
		$this->postXml($key);
		$this->result = $this->xmlToArray($this->response);
		if($this->result['return_code'] == 'SUCCESS')
		{
			return $this->result;
		}
		else
		{
			exit(json_encode(array('status'=>400, 'msg'=>$this->result['return_msg'])));
		}
	}

	/**
	 * 	作用：设置jsapi的参数
	 */
	function getParameters($appid, $partnerid, $prepayid, $key)
	{
		$apiObj['appid'] = $appid;
		$apiObj['partnerid'] = $partnerid;
		$apiObj['prepayid'] = $prepayid;
		$apiObj['package'] = 'Sign=WXPay';
		$apiObj['noncestr'] = $this->createNoncestr();
	    $apiObj['timestamp'] = (string)time();
	    $apiObj['sign'] = $this->getSign($apiObj, $key);

	    unset($apiObj['package']);
	    $apiObj['packages'] = 'Sign=WXPay';
		return json_encode($apiObj);
	}
}

?>

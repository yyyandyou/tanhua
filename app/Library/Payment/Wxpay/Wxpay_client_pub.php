<?php
/**
 * 微信支付帮助库
 * ====================================================
 * 接口分三种类型：
 * 【请求型接口】--Wxpay_client_
 * 		统一支付接口类--UnifiedOrder
 * 【响应型接口】--Wxpay_server_
 * 		通用通知接口--Notify
 * =====================================================
 * 【CommonUtil】常用工具：
 * 		trimString()，设置参数时需要用到的字符处理函数
 * 		createNoncestr()，产生随机字符串，不长于32位
 * 		formatBizQueryParaMap(),格式化参数，签名过程需要用到
 * 		getSign(),生成签名
 * 		arrayToXml(),array转xml
 * 		xmlToArray(),xml转 arrayW
*/

/**
 * 所有接口的基类
 */

namespace App\Library\Payment\Wxpay;
use App\Library\Payment\Wxpay\Common_util_pub;
/**
 * 请求型接口的基类
 */
class Wxpay_client_pub extends Common_util_pub
{
	var $parameters;	//请求参数，类型为关联数组
	public $response;	//微信返回的响应
	var $url;			//接口链接

	/**
	 * 	作用：设置请求参数
	 */
	function setParameter($parameter, $parameterValue)
	{
		$this->parameters[$this->trimString($parameter)] = $this->trimString($parameterValue);
	}

	/**
	 * 	作用：post请求xml
	 */
	function postXml($key)
	{
	    $xml = $this->createXml($key);
	    $this->response = \AppHelper::instance()->http_post_request($this->url, $xml);
		return $this->response;
	}
}

?>

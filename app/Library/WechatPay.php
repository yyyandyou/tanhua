<?php

namespace App\Library;
//namespace App\Library\Payment\Wxpay\WxClient;

use Illuminate\Database\Eloquent\Model;
use App\Library\Payment\Wxpay\UnifiedOrder_pub;
use App\Library\Payment\Wxpay\Wxpay_server_pub;

class WechatPay
{
    /**
     * 原来花聊的配置文件
    private $AppID = 'wx1848daaabe37e23a';
    private $MchID = '1481990622';
    private $Key = 'WjnTc3rWF2TRol6LsIQylccDqpyva2L5';
     */
    // 泡聊，来品品，微信支付配置
    private $AppID = '';// 'wxecea2e06758bdde4';
    private $MchID =  '';// '1505654281';
    private $notify_url = '';
    private $Key = '';
    private $returnParameters;//返回参数，类型为关联数组

    private $channel = '';
    private $account_name = '';

    /**
     * 构造函数
     */
//    function __construct()
//    {
//        $channel = cpConfig::get('appChannelName');
//        if (empty($channel)) return;
//        $wxpayaccount = cpConfig::get('wxpayaccount');
//        if (!isset($wxpayaccount[$channel])) {
//            $channel = 'default';
//        }
//
//        $channel = "banliao";
//        $accountlist = $wxpayaccount[$channel];
//
//        // 随机打乱支付账户列表，然后取一个账户
//        $rand = mt_rand(1, 100);
//        if ($rand <= 3) {
//            $account_name = 'flgchaogou';
//        } elseif ($rand > 3 && $rand <= 6) {
//            $account_name = 'youhaowu';
//        } elseif ($rand > 7 && $rand <= 10) {
//            $account_name = 'chaoliulegou';
//        } elseif ($rand > 10 && $rand <= 50) {
//            $account_name = 'gulutao';
//        } else {
//            $account_name = 'duocaitaotao';
//        }
//        $account = $accountlist[$account_name];
//
//        $this->AppID = $account['appid'];
//        $this->MchID = $account['mchid'];
//        $this->Key = $account['key'];
//        $this->channel = $channel;
//        $this->account_name = $account_name;
//
//    }

    /**
     * 支付回调的时候动态设置当然支付账号
     * @param string $channel
     * @param string $account_name
     */
    /*public function setAccountInfo($channel, $account_name)
    {
        $wxpayaccount = cpConfig::get('wxpayaccount');
        $accountlist = $wxpayaccount[$channel];

        $account = $accountlist[$account_name];
        $this->AppID = $account['appid'];
        $this->MchID = $account['mchid'];
        $this->Key = $account['key'];
        $this->channel = $channel;
        $this->account_name = $account_name;
    }*/
    /**
     * 支付回调的时候动态查询并设置支付账号
     * @param string $channel
     * @param string $account_name
     */
    public function setAccountInfoByDB($channel, $account_name, $type = 2){

        /*if ($type == 2){
            $wxpayaccount = cpConfig::get('wxpayaccount');
            $accountlist = $wxpayaccount[$channel];
            $account = $accountlist[$account_name];
            $this->AppID = $account['appid'];
            $this->MchID = $account['mchid'];
            $this->Key = $account['key'];
        }elseif($type == 6) {
            $accountall =cpConfig::get('accountall');
            $account = $accountall[$account_name];
            $this->AppID = $account['app_id'];
            $this->MchID = $account['mchid'];
            $this->Key = $account['wxkey'];
        }*/
        $account = [
            'appid' => $this->AppID,
            'mchid' => $this->MchID,
            'key' => $this->Key
        ];
        $this->channel = $channel;
        $this->account_name = $account_name;
        return $account;
    }

    /*private function initAccount($userId){
        $channel = cpConfig::get('appChannelName');
        $amount = model('recharge')->getUserRecharge($userId);
        $rand = mt_rand(1,100);

        if($channel == 'bibi'||$channel == 'jiaoliao'){
//             $config = cpConfig::get('wxpayaccount');
//             $list = $config[$channel];
//             $keys = array_keys($list);
//             $accountName = $keys[array_rand($keys)];

//             if ($rand<=70) {
//                 $accountName = 'feiniaogou';
//             } else {
//                 $accountName = 'meisushangcheng';
//             }


            $accountName ='mgou';

//             $user = model('user')->getUserById($userId);
//             if ($user['register_time']>1556640000) {
//                 $accountName = 'huoxinggou';
//             }

        } elseif ($channel=='ruanliao') {
//             $accountName = 'kktao';
//             $accountName = 'tiantianhuigou';
//             $accountName = 'youxuanhuigou';
            $accountName = 'taomanhuai';
        } else{
            $channel = 'bibi';
//             if($rand<=25) {
//                 $accountName = 'duocaitaotao';
//             } elseif ($rand<=55) {
//                 $accountName = 'qilangshuma';
//             } elseif ($rand<=60) {
//                 $accountName = 'haichaogou';
//             } elseif ($rand<=61) {
//                 $accountName = 'jingpinjie';
//             } elseif ($rand<=62) {
//                 $accountName = 'meilikuaigou';
//             } elseif ($rand<=63) {
//                 $accountName = 'meiriqingpin';
//             } elseif ($rand<=95) {
//                 $accountName = 'gulutao';
//             } else {
//                 $accountName = 'diantaotao';
//             }

            $accountName = 'pilitao';

            if (in_array($userId, [166696, 167213])) {
                //$accountName = 'meifuling';
            }
        }

        $config = cpConfig::get('wxpayaccount');
        $accountList = $config[$channel];
        $account = $accountList[$accountName];
        $this->AppID = $account['appid'];
        $this->MchID = $account['mchid'];
        $this->Key = $account['key'];
        $this->channel = $channel;
        $this->account_name = $accountName;
    }*/

    public function pay($data, $userId = 0)
    {
        //$this->initAccount($userId);

        $attach['order_id'] = $data['order_id'];
        $attach['channel'] = $this->channel;
        $attach['account_name'] = $this->account_name;

        $attachStr = http_build_query($attach);

        $unifiedOrder = new UnifiedOrder_pub(); //new UnifiedOrder_pub();
        $unifiedOrder->setParameter('appid', $this->AppID); //公众账号ID
        $unifiedOrder->setParameter('mch_id', $this->MchID); //商户号
        $unifiedOrder->setParameter('body', $data['name']); //商品描述
        $unifiedOrder->setParameter('out_trade_no', $data['order_id']); //商户订单号
        $unifiedOrder->setParameter('total_fee', $data['amount']*100); //总金额
        $unifiedOrder->setParameter('spbill_create_ip', \AppHelper::instance()->get_client_ip()); //终端IP
        $unifiedOrder->setParameter('notify_url', $this->notify_url); //通知地址
        //$unifiedOrder->setParameter('notify_url', 'https://newapp.huacall.com/api/notify_wxpay'); //通知地址
        $unifiedOrder->setParameter('trade_type', 'APP'); //交易类型
        $unifiedOrder->setParameter('attach', $attachStr);
        //$unifiedOrder->setParameter('attach', $data['order_id']);
        $prepay_id = $unifiedOrder->getPrepayId($this->Key);

        $data = $unifiedOrder->getParameters($this->AppID, $this->MchID, $prepay_id, $this->Key);

        return $data;
    }

    /*public function getArrData($postStr)
    {
        $WxpayServer = new Wxpay_server_pub();
        $WxpayServer->saveData($postStr);
        return $WxpayServer->getData();
    }*/

    public function getData($postStr)
    {
        $WxpayServer = new Wxpay_server_pub();
        $WxpayServer->saveData($postStr);

        if (!$WxpayServer->checkSign($this->Key)) {
            return false;
        }

        return $WxpayServer->getData();
    }

    public function returnxml($params)
    {
        $WxpayServer = new Wxpay_server_pub();

        if(empty($params)){
            return;
        }

        foreach ($params as $key => $value) {
            $WxpayServer->setReturnParameter($key, $value);//返回状态码
        }

        return $WxpayServer->returnXml();
    }

    /*创建微信小程序订单*/
    /*public static function createWxminiOrder($appId, $openid, $orderInfo){
        if (empty($orderInfo)) {
            return '未找到订单信息';
        }

        //$config = array_filter(cpConfig::get("wechat.payment.default"));
        $config['notify_url'] = cpConfig::get('wxpay_notify_url');
        $config['app_id'] = $appId;
        $config['mch_id'] = $orderInfo['mchid'];
        $config['key']    = $orderInfo['wxkey'];


        $attach['order_id'] = $orderInfo['order_id'];
        $attach['channel'] = $orderInfo['channel'];
        $attach['account_name'] = $orderInfo['account_name'];
        $attach['type'] = 6;
        $attachStr = http_build_query($attach);

        try{
            require_once '../system/lib/vendor/autoload.php';
            $app = \EasyWeChat\Factory::payment($config);
            $result = $app->order->unify([
                'body' => $orderInfo['order_id'],
                'out_trade_no' => $orderInfo['order_id'],
                'total_fee' => $orderInfo['price'] * 100,
                // 'total_fee' =>0.01 * 100,
                'trade_type' => 'JSAPI', // 请对应换成你的支付方式对应的值类型
                'openid' => $openid,
                'attach' => $attachStr,
            ]);

            $jssdk = $app->jssdk;
            $result = $jssdk->bridgeConfig($result['prepay_id'], false);
        }catch (\Exception $e) {
            cpError::log($e->getMessage().json_encode($attach), 'jinfuwx_error');
            return $e->getMessage();
        }

        if (strpos($orderInfo['channel'], 'ios') !== false) {
            $platform = 1;  // 1-iOS 2-Android
        }else {
            $platform = 2;  // 1-iOS 2-Android
        }

        $data = [
            'order_id' => $orderInfo['order_id'],
            'user_id' => $orderInfo['user_id'],
            'product_id' => $orderInfo['product_id'],
            'trade_type' => $orderInfo['paymod'],
            'platform' => $platform,
            'trans_type' => $orderInfo['trans_type'],
            // 'app_channel' => $orderInfo['channel'],
            'amount' => $orderInfo['price'],
            'order_time' => time()
        ];

        $pay_config = [
            'type' => 6,
            'account_name' => $orderInfo['account_name'],
            'channel' => $orderInfo['channel'],
            'app_id' => $config['app_id'],
            'mchid' => $config['mch_id'],
            'key' => $config['key'],
            'app_secret' => $config['key'],
        ];
        $data['pay_config'] = json_encode($pay_config);

        $cr['order_id']=$orderInfo['order_id'];
        $sum=model('common')->_count('transaction',$cr);
        if($sum>=1){
            return [
                'pay_info' => $result,
                'order_info' => $orderInfo
            ];
        }
        if (!model('common')->_insert('transaction', $data)) {
            cpError::log(json_encode($data), 'jinfuwx_error');
            return '订单创建失败！';
        }
        return [
            'pay_info' => $result,
            'order_info' => $orderInfo
        ];
    }*/

    /*获取微信openid*/
    /*public function getOpenId($appId, $secret, $code, $sessionKey){
        $config = [
            'app_id' => $appId,
            'secret' => $secret,
            // 下面为可选项
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',
            'log' => [
                'level' => 'debug',
                'file' => DATA_PATH.'/api/wechat_openid.log',
            ],
        ];

        $redisKey = $this->OPENID_KEY.$appId;
        if (empty($sessionKey)) {
            try{
                require_once '../system/lib/vendor/autoload.php';
                $app = \EasyWeChat\Factory::miniProgram($config);
                $res = $app->auth->session($code);
                model('redis')->hSet($redisKey, $res['session_key'], $res['openid']);
            }catch (\Exception $e) {
                $config['code'] = $code;
                cpError::write("Wxmini_session_error:".var_export($res, true).var_export($config, true));
                return '获取openid失败';
            }
            $sessionKey = $res['session_key'];
            $openid = $res['openid'];
        }else {
            $openid = model('redis')->hGet($redisKey, $sessionKey);
        }
        return [
            'openid' => $openid,
            'session_key' => $sessionKey
        ];
    }*/
}

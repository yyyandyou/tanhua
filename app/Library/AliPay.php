<?php

namespace App\Library;

use Illuminate\Database\Eloquent\Model;
use App\Library\Payment\Alipay\AopClient;
use App\Library\Payment\Alipay\request\AlipayTradeAppPayRequest;
use App\Library\Payment\Alipay\request\AlipayTradeWapPayRequest;
//use App\Library\Payment\Wxpay\UnifiedOrder_pub;
//use App\Library\Payment\Wxpay\Wxpay_server_pub;

class AliPay
{
    private $client;
    //public $AppID = '2017041306694137'; // 原花聊的
    //public $Email = '2511026056@qq.com'; // 原花聊的
//     public $AppID = '2018053160282395'; // 支付宝浪聊
//     public $Email = '3325354821@qq.com'; // 支付回调的时候回用到
    public $AppID = '2017041306694137'; // 支付宝浪聊
    public $Email = '2511026056@qq.com'; // 支付回调的时候回用到
    private $RsaPrivateKey = 'MIICXQIBAAKBgQCCAqniwr3YFP8+0wmtkjS84OddBbyY2XNNgEcDaxsNC4938/vVf6/4IoVI2MTU/CAgNAVvqQRJDjo5mdT72UrQbD/Tq/VD4pHM0N51HLWkdsiiYSdaBdiPDt5HtFyvJYXmc9ilfSfnzwoIHD1QVnNtXXk2zZOd34gak0/3IwO4eQIDAQABAoGAYqUevd3BKfkUiliaGUa8GSwUoHI/3UHwyLcyKpfT2JwS6ZMIW8Achou60cEX5/q9bXHLRiPaCoxvBVGBVu5SYqHWbpTcSEBhtk1RoEjm0UJ4gT7JZtfCpMMiGzEB3XPFSMj6F0CEXgL7uTdLQoldhXq9pLb+QgOS7qxRNzwaHQECQQDl6SDPJXWaQTAkRQ/BkQTB6tGbIwfeMrAUjCGXz1c1BBL+0T1bCD6wuOomIaX6P4/IO3xitibkP5YC9o5gN5hJAkEAkMNyhhniAct8a6ZUjK8MU8B2PZ3nf5+L2x5GGMc3pDeV/xbUYwOB6bleB+CTPdFLJVgW/+ac9KXCz0LPlaP+sQJBANbbfTvY6ASWMyVSHR1qJyYKCoSOwkWW3RL2ulYjAH6dJhtY85vlIqEIzlmATJqHWBlp7h5Z2BKqkciHHhsxGXECQGcXOKR1JDThA17qGYYCVsYB5nJk0+pX5VIK0iJtJJvGlgQN7vwAEHEwd90mOdUpCdmzTNW6FgZHUV8mFgxbuFECQQCro3nieoEpQFvnnOdA1AYdd9EKkDDUk7okWQnlOayJcw8PNtCxinZDSkYbosPzAbfTbqerOV1HxkpPrCT7d0Ff';
    private $SignType = 'RSA';
    private $AlipayrsaPublicKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCCAqniwr3YFP8+0wmtkjS84OddBbyY2XNNgEcDaxsNC4938/vVf6/4IoVI2MTU/CAgNAVvqQRJDjo5mdT72UrQbD/Tq/VD4pHM0N51HLWkdsiiYSdaBdiPDt5HtFyvJYXmc9ilfSfnzwoIHD1QVnNtXXk2zZOd34gak0/3IwO4eQIDAQAB';
    private $notify_url = URL_PATH . '/api/aliPay_notify';

    public function __construct()
    {
        //自动加载类
        spl_autoload_register(function ($classname)
        {
            $dir_array = array(
                PROJECT_PATH.'/Library/Payment/Alipay/',
                PROJECT_PATH.'/Library/Payment/Alipay/request/'
            );

            foreach($dir_array as $dir) {
                $file = $dir . $classname . '.php';
                if (is_file($file)){
                    require_once($file);
                    return;
                }
            }
        });

    }

    /**
     * 设置app支付账号
     */
    public function setAppPayAccount($data)
    {
        //$alipayaccount = cpConfig::get('alipayaccount');
        $rand = mt_rand(1, 2);

        /*if ($data['amount']>=0) {
            $account_name = 'liantu';
        }

        $account = $alipayaccount[$account_name];*/

        //model('transaction')->update(['order_id' => $data['order_id']], ['pay_account' => $account_name]);

        $this->AppID = $this->AppID;
        $this->Email = $this->Email;

        $this->client = new AopClient;
        $this->client->appId = $this->AppID;
        $this->client->rsaPrivateKey = $this->RsaPrivateKey;
        $this->client->format = 'json';
        $this->client->charset= 'UTF-8';
        $this->client->signType= $this->SignType;
        $this->client->alipayrsaPublicKey = $this->AlipayrsaPublicKey;
    }

    /**
     * 设置h5支付账号
     */
    public function setH5PayAccount($data)
    {
        //$alipayaccount = cpConfig::get('alipayaccount');
//         $rand = mt_rand(0, 9);
//         if ($rand==1) {
//             $account = $alipayaccount['leadnewnet'];
//         } else {
//             $account = $alipayaccount['jingyongtech'];
//         }

        //$account = $alipayaccount['leadnewnet'];
//         $account = $alipayaccount['gaoniunet'];
//         $account = $alipayaccount['rouchuantech'];
//         $account = $alipayaccount['lianaitech'];
//         $account = $alipayaccount['rouchuantech'];

//         $user = model('user')->getUserById($data['user_id']);
//         $register_time = $user['register_time'];
//         if ($register_time>1556640000 && $data['amount']<=70) {
//             $account = $alipayaccount['rouchuantech'];
//         } elseif ($data['amount']<=30) {
//             $account = $alipayaccount['rouchuantech'];
//         } else {
//             $account = $alipayaccount['rouchuantech'];
//         }

        //$account = $alipayaccount['qiruitech'];

        //$this->AppID = $this->AppID;
        //$this->Email = $this->Email;

        $this->client = new AopClient;
        $this->client->appId = $this->AppID;
        $this->client->rsaPrivateKey = $this->RsaPrivateKey;
        $this->client->format = 'json';
        $this->client->charset= 'UTF-8';
        $this->client->signType= $this->SignType;
        $this->client->alipayrsaPublicKey = $this->AlipayrsaPublicKey;
    }

    /**
     * 通过email来获取当前的支付账号
     * @param string $email
     */
    public function setAlipayAccount($email) {
        /*$alipayaccount = cpConfig::get('alipayaccount');
        if ($email=='3325354821@qq.com') {
            $account = $alipayaccount['leadnewnet'];
            $account_name = 'leadnewnet';
        } elseif ($email=='2261218434@qq.com') {
            $account = $alipayaccount['gzzigui'];
            $account_name = 'gzzigui';
        } elseif ($email=='1543738113@qq.com') {
            $account = $alipayaccount['qingdongtech'];
            $account_name = 'qingdongtech';
        } elseif ($email == '3080606810@qq.com') {
            $account = $alipayaccount['jingyongtech'];
            $account_name = 'jingyongtech';
        } elseif ($email=='1753266823@qq.com') {
            $account = $alipayaccount['posutech'];
            $account_name = 'posutech';
        } elseif ($email=='takego2018@163.com') {
            $account = $alipayaccount['gaoniunet'];
            $account_name = 'gaoniunet';
        }  elseif ($email=='3201371816@qq.com') {
            $account = $alipayaccount['ouketech'];
            $account_name = 'ouketech';
        } elseif ($email=='2582755003@qq.com') {
            $account = $alipayaccount['rouchuantech'];
            $account_name = 'rouchuantech';
        } elseif ($email=='2369126026@qq.com') {
            $account = $alipayaccount['yashitech'];
            $account_name = 'yashitech';
        } elseif ($email=='1457261745@qq.com') {
            $account = $alipayaccount['lianaitech'];
            $account_name = 'lianaitech';
        } elseif ($email=='1963662524@qq.com') {
            $account = $alipayaccount['yunruntech'];
            $account_name = 'yunruntech';
        } elseif ($email=='3511349627@qq.com') {
            $account = $alipayaccount['qiruitech'];
            $account_name = 'qiruitech';
        } elseif ($email=='3066894814@qq.com') {
            $account = $alipayaccount['miqichuangxiang'];
            $account_name = 'miqichuangxiang';
        } elseif ($email=='409561183@qq.com') {
            $account = $alipayaccount['mingdeshangmao'];
            $account_name = 'mingdeshangmao';
        }  elseif ($email=='laka198198@163.com') {
            $account = $alipayaccount['yijiunet'];
            $account_name = 'yijiunet';
        }  elseif ($email=='okkj456321@126.com') {
            $account = $alipayaccount['oukefushu'];
            $account_name = 'oukefushu';
        } elseif ($email=='3452891535@qq.com') {
            $account = $alipayaccount['fuyuntech'];
            $account_name = 'fuyuntech';
        } elseif ($email=='2898932154@qq.com') {
            $account = $alipayaccount['minuocheng'];
            $account_name = 'minuocheng';
        }elseif($email=='2336451383@qq.com'){
            $account = $alipayaccount['jiangcai'];
            $account_name = 'jiangcai';

        }elseif($email=='hengdu666123@163.com'){
            $account = $alipayaccount['hengdu'];
            $account_name = 'hengdu';

        }elseif($email=='1924749280@qq.com'){
            $account = $alipayaccount['aike'];
            $account_name = 'aike';

        }else{
            $account = $alipayaccount['liantu'];
            $account_name = 'liantu';
        }*/
        $account_name = '';
        $this->AppID = $this->AppID;
        $this->Email = $this->Email;

        $this->client = new AopClient;
        $this->client->appId = $this->AppID;
        $this->client->rsaPrivateKey = $this->RsaPrivateKey;
        $this->client->format = 'json';
        $this->client->charset= 'UTF-8';
        $this->client->signType= $this->SignType;
        $this->client->alipayrsaPublicKey = $this->AlipayrsaPublicKey;
        return $account_name;
    }

    //app支付接口
    public function pay($data)
    {
        $this->setAppPayAccount($data);
        $request = new AlipayTradeAppPayRequest();

        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        //$a=rand(1,100);
        $data['name'] = $data['order_id'];
        $bizcontent = array(
            'subject' => $data['name'],
            'out_trade_no' => $data['order_id'],
            'total_amount' => $data['amount'],
            'product_code' => 'QUICK_MSECURITY_PAY'
        );

//         $request->setNotifyUrl('https://newapp.huacall.com/api/notify_alipay');
        $request->setNotifyUrl($this->notify_url);
        $request->setBizContent(json_encode($bizcontent));
        //这里和普通的接口调用不同，使用的是sdkExecute
        $response = $this->client->sdkExecute($request);
        return $response;
    }


    //h5网页支付
    public function webpay($data, $returnurl)
    {
        $this->setH5PayAccount($data);
        $request = new AlipayTradeWapPayRequest();

        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        $data['name'] = '天河苑2号停车场';
        $bizcontent = array(
            'subject'       => $data['name'],
            'out_trade_no'  => $data['order_id'],
            'total_amount'  => $data['amount'],
            'product_code'  => 'QUICK_WAP_WAY'
        );

        //$request->setNotifyUrl('https://newapp.huacall.com/api/notify_alipay');
        //$request->setNotifyUrl(cpConfig::get('alipay_notify_url'));
        $request->setNotifyUrl($this->notify_url);
        $request->setReturnUrl($returnurl);
        $request->setBizContent(json_encode($bizcontent));
        $response = $this->client->pageExecute($request);
        return $response;
    }

    //异步通知验证接口
    public function rsaCheckV1($data)
    {
        return $this->client->rsaCheckV1($data, NULL, 'RSA2');
    }

    public function payment($data)
    {
        $request = new AlipayFundTransToaccountTransferRequest ();

        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        $bizcontent = array(
            'out_biz_no' => $data['order_id'],
            'payee_type' => 'ALIPAY_LOGONID',
            'payee_account' => $data['alipay_account'],
            'amount' => $data['amount'],
            'payee_real_name' => $data['alipay_name'],
            'remark' => '提现'
        );

        $request->setBizContent(json_encode($bizcontent));

        $response = $this->client->execute($request);
        return $response;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Profile;
use App\User;
use App\Match;
use App\ChatTemplate;
use App\ChatContent;
use App\ChatHistory;

class ChatController extends Controller
{
    private $page_size = 20;

    public function chat(Request $request)
    {
        //input fields
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $profile_id = isset($request->profileId)?$request->profileId:'';
        $last_chat_id = isset($request->lastChatId)?$request->lastChatId:''; //null if new chat
        $status = 200;
        $message = "";
        $content = array();
        $chat_order = 1;
        $match_id = 1;
        $message_count = 1;
        $chat_template_id = '';

        //get user ID
        $data = User::query()->where('unique_id', '=', $unique_id)->first();
        if(empty($data)) {
            $status = 400;
            $message = __('message.not_register_yet');
        }else {
            $userId = $data->id;
            $genderId = $data->gender;
            $vip_status = $data->vip_status;
            $vip_expired = $data->vip_expired;

            $match_data = Match::query()->where('profile_id', '=', $profile_id)->where('user_id', '=', $userId)->first();
            //if never match before
            if(empty($match_data)) {
                $status = 400;
                $message = __('message.profile_not_match_before');
            }else {
                $match_id = $match_data->id;

                //search chat history exist
                $chat_history = ChatHistory::query()->where('user_id', '=', $userId)
                    ->where('profile_id', '=', $profile_id)
                    ->first();

                //check if VIP
                $today = date("Y-m-d");
                if($vip_status > 0 && (date('Y-m-d', strtotime($vip_expired)) >= $today)){
                    $sql_where = ' AND ct.vip_template = 1 ';
                }else {
                    //if chat before, get next message
                    if (!empty($last_chat_id)) {
                        $last_chat_id += 1;
                    } else {
                        $last_chat_id = 1;
                    }
                    $sql_where = ' AND ct.vip_template <> 1 AND cc.chat_order = ' . $last_chat_id;

                    //get template ID
                    if(!empty($chat_history)){
                        $chat_template_id = " AND cc.template_id = ".$chat_history->template_id;
                    }
                }

                $sql = 'SELECT cc.* FROM chat_contents cc LEFT JOIN chat_templates ct ON cc.template_id = ct.id
                            WHERE ct.status = 1
                            '.$chat_template_id.'
                            AND (ct.gender = '.$genderId.' OR ct.gender = 0) '.$sql_where.'
                            ';

                $chat_data = DB::select(DB::raw($sql));
                if(empty($chat_data)){
                    $status = 400;
                    $message = __('message.chat_ended');
                }else {
                    //image root
                    $image_root = \AppHelper::instance()->getImageRoot();

                    $array_length = count($chat_data);
                    $random_key = rand(0, $array_length -1);
                    $random_content = $chat_data[$random_key];

                    if($random_content->type == 2 OR $random_content->type == 5){
                        $message_content = $image_root.$random_content->content;
                    }else{
                        $message_content = $random_content->content;
                    }

                    $content = array('message' => $message_content,
                                    'answer_1' => (empty($random_content->answer_1))?"":$random_content->answer_1,
                                    'answer_2' => (empty($random_content->answer_2))?"":$random_content->answer_2,
                                    'answer_3' => (empty($random_content->answer_3))?"":$random_content->answer_3,
                                    'answer_4' => (empty($random_content->answer_4))?"":$random_content->answer_4,
                                    'answer_5' => (empty($random_content->answer_5))?"":$random_content->answer_5,
                                    'type'     => $random_content->type
                                    );
                    $chat_order = $random_content->chat_order;

                    //get total message
                    $template_id = $random_content->template_id;
                    $message_count = ChatContent::query()->where('template_id', '=', $template_id)->count();

                    //chat exist
                    if(empty($chat_history)){
                        //insert chat into chat history
                        $chat_history = new ChatHistory();
                        $chat_history->user_id = $userId;
                        $chat_history->profile_id = $profile_id;
                        $chat_history->template_id = $random_content->template_id;
                    }
                    $chat_history->last_chat_id = $random_content->id;
                    $chat_history->updated_datetime = date('Y-m-d H:i:s');
                    $chat_history->save();

                    $message = __('message.success');
                }
            }

        }
        \AppHelper::instance()->output(array('status'=>$status, 'match_id' => $match_id, 'content'=>$content, 'chat_order' => $chat_order, 'message_count' => $message_count, 'msg' => $message));
    }

    public function getChatList(Request $request){
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $page_number = isset($request->pageNumber)?(\AppHelper::instance()->trimStr($request->pageNumber) - 1):0;
        $status = 200;
        $message = "";
        $chat_list = array();
        $offset = 0;
        $total_page = 0;

        if($page_number <= 0){
            $page_number = 0;
        }else{
            $offset = $page_number * $this->page_size;
        }

        //get user ID
        $data = User::query()->where('unique_id', '=', $unique_id)->first();
        if(empty($data)) {
            $status = 400;
            $message = __('message.not_register_yet');
        }else {
            $userId = $data->id;
            $image_root = \AppHelper::instance()->getImageRoot();

            //get total record
            $sql = "SELECT count(*) AS tot FROM chat_histories ch
                    LEFT JOIN profiles p ON p.id = ch.profile_id
                    LEFT JOIN chat_contents ct ON ct.id = ch.last_chat_id
                    WHERE user_id =". $userId;

            $total = DB::select(DB::raw($sql));
            if(!empty($total[0])) {
                $total_page = ceil($total[0]->tot / $this->page_size);
            }

            //search chat history exist
            $sql = "SELECT ch.*, p.*, ct.content FROM chat_histories ch
                    LEFT JOIN profiles p ON p.id = ch.profile_id
                    LEFT JOIN chat_contents ct ON ct.id = ch.last_chat_id
                    WHERE user_id = $userId
                    ORDER BY ch.id DESC
                    LIMIT ".$offset.",  ".$this->page_size;
            $chat_datas = DB::select(DB::raw($sql));

            if(!empty($chat_datas)){
                $i = 0;
                foreach($chat_datas as $chat_data){
                    $chat_list[$i] = array('profile_id' => $chat_data->profile_id,
                        'nickname' => $chat_data->nickname,
                        'gender' => $chat_data->gender,
                        'age' => $chat_data->age,
                        'vip_status' => $chat_data->vip_status,
                        'last_chat_message' => $chat_data->content,
                        'picture_1' => ($chat_data->picture_1 <> '')?$image_root.$chat_data->picture_1:'',
                        'picture_2' => ($chat_data->picture_2 <> '')?$image_root.$chat_data->picture_2:'',
                        'picture_3' => ($chat_data->picture_3 <> '')?$image_root.$chat_data->picture_3:''
                        );
                    $i++;
                }
            }
            $message = __('message.success');
        }

        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message, 'chat_list' => $chat_list, 'total_page' => $total_page));
    }

    public function getRandomCall(Request $request){
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $status = 200;
        $message = "";
        $profile = array();

        //get user ID
        $data = User::query()->where('unique_id', '=', $unique_id)->first();
        if(empty($data)) {
            $status = 400;
            $message = __('message.not_register_yet');
        }else {
            $userId = $data->id;
            $image_root = \AppHelper::instance()->getImageRoot();

            //get all profiles
            $sql = "SELECT m.*, p.nickname, p.picture_1, p.picture_2, p.picture_3 FROM matches m
                    LEFT JOIN profiles p ON p.id = m.profile_id
                    WHERE (m.user_id = $userId)
                    ORDER by m.match_datetime DESC LIMIT 100";
            $profiles = DB::select(DB::raw($sql));

            //get a random profile
            if(!empty($profiles)){
                $random_id = mt_rand(0, count($profiles) - 1);
                $image_root = \AppHelper::instance()->getImageRoot();
                if(isset($profiles[$random_id])) {
                    $profile['nickname'] = $profiles[$random_id]->nickname;
                    $profile['picture_1'] = empty($profiles[$random_id]->picture_1)?"":$image_root.$profiles[$random_id]->picture_1;
                    $profile['picture_2'] = empty($profiles[$random_id]->picture_2)?"":$image_root.$profiles[$random_id]->picture_2;
                    $profile['picture_3'] = empty($profiles[$random_id]->picture_3)?"":$image_root.$profiles[$random_id]->picture_3;
                }
            }
            $message = __('message.success');
        }

        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message, 'profile' => $profile));
    }

    public function debug($message = ''){
        \AppHelper::instance()->output(array('status' => 400, 'msg' => $message));
        exit;
    }

}

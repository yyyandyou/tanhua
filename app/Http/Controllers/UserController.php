<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\User;
use App\Bookmark;
use App\Match;

class UserController extends Controller
{
    protected $config = array();
    protected $user = array();
    private $_data = array();
    private $device = 'android';
    private $APP_PASSCODE = '3iKlTscZ14s=rCh4qHzgxYjM4ATV4cJD5';
    private $page_size = 20;

    public function register(Request $request)
    {
        //input fields
        $gender = isset($request->gender)?\AppHelper::instance()->trimStr($request->gender, 'int'):'';
        //$age = $request->age;
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $status = 200;
        $message = "";
        $can_register = 1;

        //gender cannot be null
        if(!empty($gender) && !empty($unique_id)){

            //check user register before or not
            $data = User::query()->where('unique_id', '=', $unique_id)->first();

            //remove all user data in exist
            if(!empty($data)){
                if($data->phone == 0) {
                    User::where('unique_id', '=', $unique_id)->delete();
                }else{
                    $can_register = 0;
                }
            }

            if($can_register) {
                //register new user
                $user = new User;
                $user->gender = $gender;
                // $user->age = $age;
                $user->unique_id = $unique_id;
                $user->created_datetime = date("Y-m-d H:i:s");

                try {
                    $user->save();
                    $message = __('message.success_register');
                } catch (QueryException  $ex) {
                    Log::error($ex->getMessage());
                    $status = 400;
                    $message = __('message.register_fail');
                }
            }else{
                $status = 400;
                $message = __('message.already_register');
            }
        }else {
            $status = 400;
            $message = __('message.required', ['attribute' => '性别']);
        }
        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message));
    }

    public function update(Request $request)
    {
        //input fields
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        //$gender = $request->gender;
        $age = isset($request->age)?\AppHelper::instance()->trimStr($request->age, 'int'):'';
        $nickname = isset($request->nickname)?$request->nickname:'';
        $zodiac = isset($request->zodiac)?\AppHelper::instance()->trimStr($request->zodiac, 'int'):'';
        $height = isset($request->height)?\AppHelper::instance()->trimStr($request->height, 'int'):'';
        $weight = isset($request->weight)?\AppHelper::instance()->trimStr($request->weight, 'int'):'';
        $relationship = isset($request->relationship)? \AppHelper::instance()->trimStr($request->relationship, 'int'):'';
        $picture_1 = !empty($request->picture_1)?\AppHelper::instance()->trimStr($request->picture_1):'';
        $picture_2 = !empty($request->picture_2)?\AppHelper::instance()->trimStr($request->picture_2):'';
        $picture_3 = !empty($request->picture_3)?\AppHelper::instance()->trimStr($request->picture_3):'';
        $status = 200;
        $message = "";

        //$this->debug($picture_1);
        //age and gender cannot be null
        if(!empty($unique_id)){

            $user = User::query()->where('unique_id', '=', $unique_id)->first();

            //check user exist or not
            if(empty($user)) {
                $status = 400;
                $message = __('message.not_register_yet');
            }else {
                //update user
                //$user->gender = $gender;
                //$user->unique_id = $unique_id;
                $user->age = !empty($age)?$age:$user->age;
                $user->nickname = !empty($nickname)?$nickname:$user->nickname;
                $user->zodiac = !empty($zodiac)?$zodiac:$user->zodiac;
                $user->height = !empty($height)?$height:$user->height;
                $user->weight = !empty($weight)?$weight:$user->weight;
                $user->relationship = !empty($relationship)?$relationship:$user->relationship;
                $user->updated_datetime = date("Y-m-d H:i:s");

                //upload photo
                if(strlen($picture_1) > 0){
                    $random_number = \AppHelper::instance()->generateRandom(12);
                    $imgname = '/images/avatars/'.$random_number.'.jpg';
                    $imgfullname = public_path().$imgname;
                    $imsrc = base64_decode($picture_1);

                    $fp = fopen($imgfullname, 'w');
                    fwrite($fp, $imsrc);
                    if(fclose($fp)){
                        $user->picture_1 = $imgname;
                    }else{
                        $status = 400;
                        $message = __('message.image_upload_error');
                    }
                }else{
                    //$user->picture_1 = '';
                }

                if(strlen($picture_2) > 0){
                    $random_number = \AppHelper::instance()->generateRandom(12);
                    $imgname = '/images/avatars/'.$random_number.'.jpg';
                    $imgfullname = public_path().$imgname;
                    $imsrc = base64_decode($picture_2);

                    $fp = fopen($imgfullname, 'w');
                    fwrite($fp, $imsrc);
                    if(fclose($fp)){
                        $user->picture_2 = $imgname;
                    }else{
                        $status = 400;
                        $message = __('message.image_upload_error');
                    }
                }else{
                    //$user->picture_2 = '';
                }

                if(strlen($picture_3) > 0){
                    $random_number = \AppHelper::instance()->generateRandom(12);
                    $imgname = '/images/avatars/'.$random_number.'.jpg';
                    $imgfullname = public_path().$imgname;
                    $imsrc = base64_decode($picture_3);

                    $fp = fopen($imgfullname, 'w');
                    fwrite($fp, $imsrc);
                    if(fclose($fp)){
                        $user->picture_3 = $imgname;
                    }else{
                        $status = 400;
                        $message = __('message.image_upload_error');
                    }
                }else{
                    //$user->picture_3 = '';
                }

                if($status <> 400) {
                    try {
                        $user->save();
                        $message = __('message.success_update');
                    } catch (QueryException  $ex) {
                        Log::error($ex->getMessage());
                        $status = 400;
                        $message = __('message.update_fail');
                    }
                }
            }
        }else {
            $status = 400;
            $message = __('message.required', ['attribute' => 'ID']);
        }

        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message));
    }

    public function updateZodiac(Request $request)
    {
        //input fields
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $zodiac = isset($request->zodiac)?\AppHelper::instance()->trimStr($request->zodiac, 'int'):'';
        $status = 200;
        $message = "";

        //unique_id, zodiac cannot be null
        if(!empty($unique_id) && !empty($zodiac)){

            $user = User::query()->where('unique_id', '=', $unique_id)->first();

            //check user exist or not
            if(empty($user)) {
                $status = 400;
                $message = __('message.not_register_yet');
            }else {
                //update user
                $user->zodiac = !empty($zodiac)?$zodiac:$user->zodiac;
                $user->updated_datetime = date("Y-m-d H:i:s");

                if($status <> 400) {
                    try {
                        $user->save();
                        $message = __('message.success_update');
                    } catch (QueryException  $ex) {
                        Log::error($ex->getMessage());
                        $status = 400;
                        $message = __('message.update_fail');
                    }
                }
            }
        }else {
            $status = 400;
            $message = __('message.required', ['attribute' => '星座']);
        }

        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message));
    }

    /*public function updateSilent(Request $request)
    {
        //input fields
        $unique_id = $request->uniqueId;
        $silent = isset($request->silent)?(int)$request->silent:'';
        $status = 200;
        $message = "";

        //unique_id cannot be null
        if(!empty($unique_id) && !empty($silent)){

            $user = User::query()->where('unique_id', '=', $unique_id)->first();

            //check user exist or not
            if(empty($user)) {
                $status = 400;
                $message = __('message.not_register_yet');
            }else {
                //update user
                $user->silent = !empty($silent)?$silent:$user->silent;
                $user->updated_datetime = date("Y-m-d H:i:s");

                if($status <> 400) {
                    try {
                        $user->save();
                        $message = __('message.success_update');
                    } catch (QueryException  $ex) {
                        Log::error($ex->getMessage());
                        $status = 400;
                        $message = __('message.update_fail');
                    }
                }
            }
        }else {
            $status = 400;
            $message = __('message.required', ['attribute' => '提醒铃声']);
        }

        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message));
    }*/

    public function updatePassword(Request $request)
    {
        //input fields
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $phone = isset($request->phone)?\AppHelper::instance()->trimStr($request->phone):'';
        $password = isset($request->password)?$request->password:'';
        $status = 200;
        $message = "";

        //unique_id, phone, password cannot be null
        if(!empty($unique_id) && !empty($phone) && !empty($password)){

            //check china phone number
            if(!\AppHelper::instance()->checkMobile($phone)){
                $status = 400;
                $message = __('message.invalid_phone_number');
            }else {

                $user = User::query()->where('unique_id', '=', $unique_id)->first();

                //check user exist or not
                if (empty($user)) {
                    $status = 400;
                    $message = __('message.not_register_yet');
                } else {
                    //update user
                    $user->phone = $phone;
                    $user->password = md5($password);
                    $user->updated_datetime = date("Y-m-d H:i:s");

                    if ($status <> 400) {
                        try {
                            $user->save();
                            $message = __('message.success_update');
                        } catch (QueryException  $ex) {
                            Log::error($ex->getMessage());
                            $status = 400;
                            $message = __('message.update_fail');
                        }
                    }
                }
            }
        }else {
            $status = 400;
            $message = __('message.required', ['attribute' => '手机号与密码']);
        }

        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message));
    }

    public function login(Request $request)
    {
        //input fields
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $phone = isset($request->phone)?$request->phone:'';
        $password = isset($request->password)?$request->password:'';
        $status = 200;
        $message = "";

        //unique_id, phone, password cannot be null
        if(!empty($unique_id) && !empty($phone) && !empty($password)){

            $user = User::query()->where('phone', '=', $phone)
                                //->where('password', '=', md5($password))
                                ->first();

            //check user exist or not
            if(empty($user)) {
                $status = 400;
                $message = __('message.not_register_yet');
            }elseif(md5($password) == $user->password){
                //update unique ID if different
                if($unique_id <> $user->unique_id){
                    $user->unique_id = $unique_id;
                    $user->save();
                }
                $message = __('message.success_login');
            }else {
                $status = 400;
                $message = __('message.invalid_login');
            }
        }else {
            $status = 400;
            $message = __('message.required', ['attribute' => '手机号与密码']);
        }

        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message));
    }

    //获取用户信息
    public function info(Request $request)
    {
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $status = 200;
        $message = "success";
        $data = array();

        $user = User::query()->where('unique_id', '=', $unique_id)->first();

        if(!empty($user))
        {
            $image_root = \AppHelper::instance()->getImageRoot();

            $data['nickname'] = (empty($user->nickname))?"":$user->nickname;
            $data['id'] = $user->id;
            $data['gender'] = $user->gender;
            $data['phone'] = $user->phone;
            $data['age'] = $user->age;
            $data['height'] = $user->height;
            $data['weight'] = $user->weight;
            $data['relationship'] = $user->relationship;
            $data['zodiac'] = $user->zodiac;
            $data['vip_status'] = $user->vip_status;
            $data['vip_expired'] = (empty($user->vip_expired))?"":$user->vip_expired;
            $data['silent'] = $user->silent;
            $data['picture_1'] = ($user->picture_1 <> '')?$image_root.$user->picture_1:'';
            $data['picture_2'] = ($user->picture_2 <> '')?$image_root.$user->picture_2:'';
            $data['picture_3'] = ($user->picture_3 <> '')?$image_root.$user->picture_3:'';

        }else{
            $status = 400;
            $message = __('message.user_not_exist');
        }

        \AppHelper::instance()->output(array('status'=>$status, 'user'=>$data, 'msg' => $message));
    }

    public function generateUniqueID(Request $request){

        $passcode = isset($request->passcode)?$request->passcode:'';
        $success = 0;
        $status = 200;
        $unique_id = '';
        $message = __('message.success');

        if(empty($passcode) or ($passcode <> $this->APP_PASSCODE)){
            $status = 400;
            $message = __('message.invalid_access');
        }else {
            while ($success == 0) {
                $unique_id = \AppHelper::instance()->generateRandom(20);

                //search and make sure it is not exist
                $user = User::query()->where('unique_id', '=', $unique_id)->first();

                //check user exist or not
                if (empty($user)) {
                    $success = 1;
                    break;
                }
            }
        }
        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message, 'unique_id' => $unique_id));
    }

    public function myBookmark(Request $request){
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $page_number = isset($request->pageNumber)?(\AppHelper::instance()->trimStr($request->pageNumber) - 1):0;
        $status = 200;
        $message = "success";
        $bookmark = array();
        $offset = 0;
        $total_page = 0;

        $user = User::query()->where('unique_id', '=', $unique_id)->first();

        if($page_number <= 0){
            $page_number = 0;
        }else{
            $offset = $page_number * $this->page_size;
        }

        if(!empty($user))
        {
            $userId = $user->id;

            //get total record
            $sql = "SELECT count(*) AS tot FROM bookmarks b LEFT JOIN profiles p ON p.id = b.profile_id
                    WHERE b.status = 1 AND b.user_id = ".$userId;

            $total = DB::select(DB::raw($sql));
            if(!empty($total[0])) {
                $total_page = ceil($total[0]->tot / $this->page_size);
            }

            //get user bookmark
            $sql = "SELECT b.*, p.* FROM bookmarks b LEFT JOIN profiles p ON p.id = b.profile_id
                    WHERE b.status = 1 AND b.user_id = ".$userId.
                " LIMIT ".$offset.",  20";
            $data = DB::select(DB::raw($sql));
            if(!empty($data))
            {
                $i = 0;
                $image_root = \AppHelper::instance()->getImageRoot();
                foreach($data as $array){
                    $bookmark[$i] = array('profile_id' => $array->id,
                                        'nickname' => $array->nickname,
                                        'gender' => $array->gender,
                                        'height' => $array->height,
                                        'weight' => $array->weight,
                                        'relationship' => $array->relationship,
                                        'age' => $array->age,
                                        'zodiac' => $array->zodiac,
                                        /*'picture_1' => $array->picture_1,
                                        'picture_2' => $array->picture_2,
                                        'picture_3' => $array->picture_3*/
                                        'picture_1' => ($array->picture_1 <> '')?$image_root.$array->picture_1:'',
                                        'picture_2' => ($array->picture_2 <> '')?$image_root.$array->picture_2:'',
                                        'picture_3' => ($array->picture_3 <> '')?$image_root.$array->picture_3:''
                                        );
                    $i++;
                }
            }

        }else{
            $status = 400;
            $message = __('message.user_not_exist');
        }
        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message, 'bookmark' => $bookmark, 'total_page' => $total_page));
    }

    public function bookmarkProfile(Request $request){
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $profile_id = isset($request->profileId)?$request->profileId:'';
        $status = 200;
        $message = "success";

        $user = User::query()->where('unique_id', '=', $unique_id)->first();

        if(!empty($user))
        {
            $userId = $user->id;
            //get user bookmark
            $bookmark_data = Bookmark::query()->where('profile_id', '=', $profile_id)
                ->where('user_id', '=', $userId)
                ->where('status', '=', 1)
                ->first();

            if(empty($bookmark_data))
            {
                $bookmark = new Bookmark();
                $bookmark->user_id = $userId;
                $bookmark->profile_id = $profile_id;
                $bookmark->status = 1;

                try {
                    $bookmark->save();
                } catch (QueryException  $ex) {
                    Log::error($ex->getMessage());
                    $status = 400;
                    $message = __('message.update_fail');
                }
            }else{
                $status = 400;
                $message = __('message.profile_bookmark_before');
            }

        }else{
            $status = 400;
            $message = __('message.user_not_exist');
        }
        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message));
    }

    public function debug($message = ''){
        \AppHelper::instance()->output(array('status' => 400, 'msg' => $message));
        exit;
    }

}

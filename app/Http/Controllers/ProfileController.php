<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Profile;
use App\User;
use App\Match;
use App\Bookmark;

class ProfileController extends Controller
{
    private $page_size = 20;
    private $order_by = " ORDER BY p.id";
    private $random_match = 5;

    public function getProfiles(Request $request)
    {
        //input fields
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $page_number = isset($request->pageNumber)?(\AppHelper::instance()->trimStr($request->pageNumber) - 1):0;
        $random_profile = isset($request->random)?$request->random:0;
        $status = 200;
        $message = "";
        $profiles = array();
        $offset = 0;
        $total_page = 0;

        if($page_number <= 0){
            $page_number = 0;
        }else{
            $offset = $page_number * $this->page_size;
        }

        //get user ID
        $data = User::query()->where('unique_id', '=', $unique_id)->first();
        if(empty($data)) {
            $status = 400;
            $message = __('message.not_register_yet');
        }else {
            $userId = $data->id;
            $genderId = $data->gender;

            //get total record
            $sql = "SELECT count(*) AS tot FROM profiles p WHERE (p.id NOT IN (SELECT profile_id FROM matches WHERE profile_id = p.id AND user_id = $userId))
                    AND gender <>" .$genderId;
            $total = DB::select(DB::raw($sql));
            if(!empty($total[0])) {
                $total_page = ceil($total[0]->tot / $this->page_size);
            }

            if($random_profile){
                $order_by = $this->order_by . " DESC";
            }else{
                $order_by = $this->order_by;
            }

            $sql = "SELECT p.* FROM profiles p WHERE (p.id NOT IN (SELECT profile_id FROM matches WHERE profile_id = p.id AND user_id = $userId))
                    AND gender <>" .$genderId. $order_by .
                " LIMIT ".$offset.",  ".$this->page_size;
            $profiles = DB::select(DB::raw($sql));
            if(!empty($profiles)){
                $image_root = \AppHelper::instance()->getImageRoot();
                foreach($profiles as $profile){
                    if($profile->picture_1 <> ''){
                        $profile->picture_1 = $image_root.$profile->picture_1;
                    }
                    if($profile->picture_2 <> ''){
                        $profile->picture_2 = $image_root.$profile->picture_2;
                    }
                    if($profile->picture_3 <> ''){
                        $profile->picture_3 = $image_root.$profile->picture_3;
                    }
                }
            }
            $message = __('message.success');

        }
        \AppHelper::instance()->output(array('status'=>$status, 'profiles'=>$profiles, 'total_page' => $total_page, 'msg' => $message));
    }

    public function getSingleProfile(Request $request)
    {
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $profile_id = isset($request->profileId)?$request->profileId:'';
        $status = 200;
        $message = '';
        $profile = array();
        $is_bookmark = 0;

        //get user ID
        $data = User::query()->where('unique_id', '=', $unique_id)->first();
        if(empty($data)) {
            $status = 400;
            $message = __('message.not_register_yet');
        }else {
            $userId = $data->id;
            $profile_data = Profile::query()->where('id', '=', $profile_id)->first();

            if(!empty($profile_data)){
                $image_root = \AppHelper::instance()->getImageRoot();
                $profile = array('id' => $profile_data->id,
                                'nickname' => $profile_data->nickname,
                                'gender' => $profile_data->gender,
                                'age' => $profile_data->age,
                                'height' => $profile_data-> height,
                                'weight' => $profile_data-> weight,
                                'relationship' => $profile_data-> relationship,
                                'zodiac' => $profile_data-> zodiac,
                                'vip_status' => $profile_data->vip_status,
                                'distance' => $profile_data->distance,
                                'picture_1' => ($profile_data->picture_1 <> '')?$image_root.$profile_data->picture_1:'',
                                'picture_2' => ($profile_data->picture_2 <> '')?$image_root.$profile_data->picture_2:'',
                                'picture_3' => ($profile_data->picture_3 <> '')?$image_root.$profile_data->picture_3:'');
            }

            //check is bookmark profile
            $bookmark_data = Bookmark::query()->where('profile_id', '=', $profile_id)
                ->where('user_id', '=', $userId)
                ->where('status', '=', 1)
                ->first();

            if(!empty($bookmark_data))
            {
                $is_bookmark = 1;
            }
            $message = __('message.success');

        }
        \AppHelper::instance()->output(array('status'=>$status, 'profile'=>$profile, 'is_bookmark' => $is_bookmark, 'msg' => $message));
    }

    public function responseProfile(Request $request)
    {
        //input fields
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $profile_id = isset($request->profileId)?$request->profileId:'';
        $response = isset($request->response)?$request->response:'';
        $status = 200;
        $response_array = [1, 2];
        if(!in_array($response, $response_array)){
            $response = 2;
        }
        $match_status = 0;
        $message = '';
        $nickname = '';
        $picture = '';

        //check unique ID
        $data = User::query()->where('unique_id', '=', $unique_id)->first();
        if(empty($data)) {
            $status = 400;
            $message = __('message.not_register_yet');
        }else {
            $userId = $data->id;
            //check user match before
            $match_data = Match::query()->where('profile_id', '=', $profile_id)
                                        ->where('user_id', '=', $userId)
                                        //->where('status', '=', 3)
                                        ->first();
            //if never match before
            if(empty($match_data)) {
                $match = new Match();
                $match->user_id = $userId;
                $match->profile_id = $profile_id;
                $match->match_datetime = date("Y-m-d H:i:s");

                //only random match if LIKE
                if($response == 1) {
                    //random match
                    $random_num = \AppHelper::instance()->generateRandom(1, true);
                    if ($random_num > $this->random_match) {
                        $response = 3;
                        $match_status = 1;

                        //get profile info
                        $profile_data = Profile::query()->where('id', '=', $profile_id)->first();

                        if(!empty($profile_data)){
                            $image_root = \AppHelper::instance()->getImageRoot();
                            $nickname = $profile_data->nickname;
                            $picture = ($profile_data->picture_1 <> '')?$image_root.$profile_data->picture_1:'';
                        }
                    }
                }
                $match->status = $response;

                try {
                    $match->save();
                    $message = __('message.success');

                    //get user gender
                    $user_data = User::select('gender')->where('id', '=', $userId)->first();

                    //reset unmatch profile if no more profile to show
                    $sql = "SELECT p.* FROM profiles p WHERE (p.id NOT IN (SELECT profile_id FROM matches
                    WHERE profile_id = p.id AND user_id = $userId))
                    AND gender <>" .$user_data->gender;
                    $profiles = DB::select(DB::raw($sql));

                    //remove unlike match data if no more profile
                    if(empty($profiles)){
                        DB::table('matches')->where('user_id', '=', $userId)
                            ->where('status', '=', 2)
                            ->delete();
                    }

                } catch (QueryException  $ex) {
                    Log::error($ex->getMessage());
                    $status = 400;
                    $message = __('message.register_fail');
                }
            }else{
                $status = 400;
                $message = __('message.profile_response_before');
            }
        }
        \AppHelper::instance()->output(array('status'=>$status, 'match'=>$match_status, 'profileId' => $profile_id,
            'nickname' => $nickname, 'picture' => $picture, 'msg' => $message));
    }

    public function debug($message = ''){
        \AppHelper::instance()->output(array('status' => 400, 'msg' => $message));
        exit;
    }

}

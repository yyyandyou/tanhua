<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VipPackage;

class VipPackageController extends Controller
{

    //获取信息
    public function info()
    {

        $package = VipPackage::query()->get();
        $message = __('message.success');

        \AppHelper::instance()->output(array('status'=>200, 'package'=>$package, 'msg' => $message));
    }

    public function debug($message = ''){
        \AppHelper::instance()->output(array('status' => 400, 'msg' => $message));
        exit;
    }

}

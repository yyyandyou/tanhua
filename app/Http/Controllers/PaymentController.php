<?php

namespace App\Http\Controllers;

use App\Library\AliPay;
use App\Library\JinFu;
use App\Library\WechatPay;
use App\User;
use Facade\Ignition\Support\Packagist\Package;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Transaction;
use App\VipPackage;
use Illuminate\Support\Facades\Log;
use Exception;

class PaymentController extends Controller
{

    public function getAllPaymentModes(Request $request)
    {
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $status = 200;
        $message = '';
        $payment_modes = array();

        $user = User::query()->where('unique_id', '=', $unique_id)->first();
        //check user exist or not
        if (empty($user)) {
            $status = 400;
            $message = __('message.not_register_yet');
        } else{
            $message = __('message.success');
            $payment_modes = \AppHelper::instance()->allPaymentModes();
        }
        \AppHelper::instance()->output(array('status'=>$status, 'paymentModes'=>$payment_modes, 'msg' => $message));
    }

    //生成订单
    public function commit(Request $request)
    {
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $payment_type = isset($request->paymentMode)?$request->paymentMode:'';
        $package_id = isset($request->packageId)?$request->packageId:'';
        $status = 200;
        $message = '';
        $data = array();
        $order_id = '';
        $url = '';

        $user = User::query()->where('unique_id', '=', $unique_id)->first();
        $package = VipPackage::query()->where('id', '=', $package_id)->first();

        //check user exist or not
        if(empty($user)) {
            $status = 400;
            $message = __('message.not_register_yet');
        }else if(empty($package)){
            $status = 400;
            $message = __('message.invalid_package');
        }else{
            //get all payment modes
            $all_payments = \AppHelper::instance()->allPaymentModes();
            //check valid payment mode
            if(!in_array($payment_type, $all_payments)){
                $status = 400;
                $message = __('message.invalid_pay_mode');
            }else {
                //proceed to payment
                $jinfu = new JinFu();
                //生成订单信息
                $transaction = new Transaction();
                $transaction->order_id = \AppHelper::instance()->buildOrderNo($user->id);
                $transaction->user_id       = $user->id;
                $transaction->package_id    = $package->id;
                $transaction->amount        = $package->price;
                $transaction->trade_type    = $payment_type;
                $transaction->platform      = 1;
                $transaction->order_datetime    = date('Y-m-d H:i:s');
                $transaction->secret_key = $jinfu->key;
                $transaction->save();
                $message = __('message.success');

                $data = array(
                    'order_id'      => $transaction->order_id,
                    'name'          => $package->name.'套餐',
                    'user_id'       => $user->id,
                    'package_id'    => $package->id,
                    'amount'        => $package->price,
                    'trade_type'    => $payment_type,
                    'platform'      => 1,
                    'order_time'    => $transaction->order_datetime
                );
                $payment_result = \AppHelper::instance()->processPayment($payment_type, $user->id, $data);
                $status = $payment_result['status'];
                //$message = $payment_result['message'];
                $url = $payment_result['url'];
                $order_id = $transaction->order_id;
            }
        }

        \AppHelper::instance()->output(array('status'=>$status, 'url'=>$url, 'order_id' => $order_id, 'msg' => $message));
    }
    public function redirectH5pay(Request $request)
    {
        $order_id = isset($request->orderId)?$request->orderId:'';
        $token = isset($request->token)?$request->token:'';
        $transaction = Transaction::query()->where('order_id', '=', $order_id)->first();

        if(empty($transaction) OR empty($token)){
            echo __('message.order_no_not_exist');
        }else{
            //check valid token
            $ori_token = md5($transaction->order_id.number_format($transaction->amount, 2).$transaction->order_datetime);

            if($token <> $ori_token){
                echo __('message.rsa_error');
            }else {
                $package = VipPackage::query()->where('id', '=', $transaction->package_id)->first();
                $data = array(
                    'order_id' => $transaction->order_id,
                    'name' => $package->name . '套餐',
                    'user_id' => $transaction->user_id,
                    'package_id' => $package->id,
                    'amount' => $package->price,
                    'trade_type' => $transaction->tarde_type,
                    'platform' => 1,
                    'order_time' => $transaction->order_datetime
                );

                $jinFu = new JinFu();
                $paydata = $jinFu->alipay($data);
                $payurl = $jinFu->getAlipayUrl();
                return view('h5pay', ['paydata' => $paydata, 'payurl' => $payurl]);
            }
        }
    }

    //金福回调
    public function jinfuNotify(Request $request){
        try {
            $notify_data = var_export($_REQUEST, true);
            //cpError::log($notify_data, 'jinfu_log');
            Log::error($notify_data, array('jinfu_notify_log'));
            $out_trade_no = $request->out_trade_no ;//商户系统订单号，原样返回

            //$transaction = model('transaction');
            //$order = $transaction->getOrderByOrderID($out_trade_no);
            $order = Transaction::query()->where('order_id', '=', $out_trade_no)->first();

            if(empty($order)){
               // throw new Exception('不存在此订单'.$out_trade_no);
                Log::error(__('message.order_no_not_exist').$out_trade_no, array('jinfu_notify_error'));
                exit;
            }
            $mch_id = $request->mch_id;
            $key = $order->secret_key;


            $trade_state = $request->trade_state ;//1：支付成功，0：未支付，2：失败，3：已退款
            $fee_type = "CNY" ;//默认人民币：CNY
            $pay_type = $request->pay_type ;//支付类型 ，比如 201 202
            $total_amount = $request->total_amount ;//订单金额，单位为分
            $receipt_amount = $request->receipt_amount / 100 ;//实际付款金额，单位为分
            $sys_trade_no = $request->sys_trade_no ;//平台系统内的订单号
            $txn_id = $request->txn_id ;//微信、支付宝、QQ、银联、京东等官方订单号
            $device_info = $request->device_info ;//终端设备号
            $attach = $request->attach;//商户附加信息
            $time_end = $request->time_end ;//支付完成时间，格式为yyyyMMddHHmmss
            $sign = $request->sign ;//MD5签名，32位小写

            $signSource = "mch_id={$mch_id}&out_trade_no={$out_trade_no}&fee_type={$fee_type}&pay_type={$pay_type}&total_amount={$total_amount}&device_info={$device_info}&key={$key}";
            if ($sign != md5($signSource)) {
                $this->updateOrderFail($order);
                //cpError::log("Sign Error".json_encode($_POST), 'jinfu_error');
                Log::error(__('message.rsa_error').json_encode($_POST), array('jinfu_notify_error'));
                echo('FAIL');exit;
            }

            if ($trade_state != 1) {
                $this->updateOrderFail($order);
                //cpError::log("trade_state != 1".json_encode($_POST), 'jinfu_error');
                Log::error('trade_state != 1. '.$out_trade_no, array('jinfu_notify_error'));
                echo('success');exit;
            }

            if(empty($order)){
                //throw new Exception('不存在此订单'.$out_trade_no);
                Log::error(__('message.order_no_not_exist').$out_trade_no, array('jinfu_notify_error'));
                exit;
            }elseif($order->amount != $receipt_amount){
                $this->updateOrderFail($order);
                //throw new Exception('订单金额不对');
                Log::error(__('message.invalid_amount').$out_trade_no, array('jinfu_notify_error'));
                exit;
            }/*elseif ($order['deliver_status'] == 1) {
                //cpError::log("deliver_status = 1".json_encode($_POST), 'jinfu_error');
                Log::error('deliver_status = 1. '.$out_trade_no, array('jinfu_notify_log'));
                echo('success');exit;
            }*/
            //$caclDeductionData =$transaction->caclDeduction($order['user_id']);
            /*if(!$transaction->update(['trans_id' => $order['trans_id']], [
                'trade_no'=>$sys_trade_no,
                'pay_status'=>'1',
                'pay_account'=>$mch_id,
                'trans_time'=>time(),
                'flag'=>$caclDeductionData['flag'],
                'source'=>$caclDeductionData['source']
            ])){
                throw new Exception('订单状态改变失败');
            }*/
            $order->trade_no = $sys_trade_no;
            $order->pay_status = 1;
            $order->pay_account = $mch_id;
            $order->trans_datetime = date('Y-m-d H:i:s');

            try {
                $order->save();
                Log::error(__('message.success_update'), array('jinfu_notify_success'));

                //upgrade member to VIP
                $user_id = $order->user_id;
                $package_id = $order->package_id;
                //get package duration
                $package_data = VipPackage::query()->where('id', '=', $package_id)->first();

                if(!empty($package_data)){
                    $duration = $package_data->duration;
                }else{
                    $duration = 30;
                }
                $user_data = User::query()->where('id', '=', $user_id)->first();
                if(!empty($user_data)){
                    $user_data->vip_status = 1;
                    $date_now = date('Y-m-d H:i:s');
                    $user_data->vip_expired = date('Y-m-d H:i:s', strtotime($date_now. ' + '.$duration.' days'));
                    $user_data->save();
                }
            } catch (QueryException  $ex) {
                Log::error($ex->getMessage());
            }
            /*if($order['trans_type']==1){
                $msg='恭喜充值vip成功！';
            }else{
                $msg='恭喜您，充值已成功，请点击其他页面进行刷新，可查看最新金币余额';
            }*/

            //充值推送
            //通过小秘书给被关注着发送消息
            //model("chat")->sendSECMsg($order['user_id'],$msg);
            //记录日志
            //cpError::log(json_encode($_POST), 'jinfu_success');
            Log::error(json_encode($_POST), array('jinfu_notify_success'));
            //此处作逻辑处理
            echo('success');exit;
        } catch (Exception $e) {
            $this->updateOrderFail($order);
            //记录错误日志
            //cpError::log($e->getMessage().json_encode($_POST), 'jinfu_error');
            Log::error($e->getMessage().json_encode($_POST), array('jinfu_notify_error'));
            echo('FAIL');exit;
        }
    }

    public function updateOrderFail($order)
    {
        if(!empty($order)){
            $order->pay_status = -1;
            $order->trans_datetime = date('Y-m-d H:i:s');
            $order->save();
        }
    }

    //支付宝回调
    public function alipayNotify(Request $request)
    {
        try {
            $notify_data = var_export($_POST, true);
            Log::error($notify_data, array('alipay_log'));

            $seller_email = $request->seller_email;
            $auth_app_id = $request->auth_app_id;
            $trade_status = $request->trade_status;
            $out_trade_no = $request->out_trade_no;
            $total_amount = $request->total_amount;
            $trade_no = $request->trade_no;

            $aliPay = new AliPay();
            $account_name = $aliPay->setAlipayAccount($seller_email);

            //获取参数、验参
            if(!$aliPay->rsaCheckV1($_POST)){
                Log::error(__('message.rsa_error'), array('alipay_error'));
                exit;
            }elseif($aliPay->AppID != $auth_app_id){
                Log::error(__('message.invalid_app_id'), array('alipay_error'));
                exit;
            }elseif($aliPay->Email != $seller_email){
                Log::error(__('message.invalid_alipay_account'), array('alipay_error'));
                exit;
            }elseif($trade_status != 'TRADE_SUCCESS'){
                Log::error(__('message.transaction_closed'), array('alipay_error'));
                exit;
            }

            $order_id = $out_trade_no;
            $order = Transaction::query()->where('order_id', '=', $order_id)->first();
            if(empty($order)){
                Log::error(__('message.order_no_not_exist').$order_id, array('alipay_error'));
                exit;
            }elseif($order->amount != $total_amount){
                Log::error(__('message.invalid_amount').$order_id, array('alipay_error'));
                exit;
            }

            //$caclDeductionData =$transaction->caclDeduction($order['user_id']);
            /*if(!$transaction->update(array('trans_id'=>$order['trans_id']), array('trade_no'=>$_POST['trade_no'], 'pay_status'=>'1', 'pay_account'=>$account_name, 'trans_time'=>time(), 'flag'=>$caclDeductionData['flag'], 'source'=>$caclDeductionData['source']))){
                throw new Exception('订单状态改变失败');
            }
            if($order['trans_type']==1){
                $msg='恭喜充值vip成功！';
            }else{
                $msg='恭喜您，充值已成功，请点击其他页面进行刷新，可查看最新金币余额';
            }*/

            $order->trade_no = $trade_no;
            $order->pay_status = 1;
            $order->pay_account = $account_name;
            $order->trans_datetime = date('Y-m-d H:i:s');

            try {
                $order->save();
                Log::error(__('message.success_update'), array('transaction_success_update'));
            } catch (QueryException  $ex) {
                Log::error($ex->getMessage());
            }

            //充值推送
            //通过小秘书给被关注着发送消息
            //model("chat")->sendSECMsg($order['user_id'],$msg);

            //记录日志
            Log::error(json_encode($_POST), array('alipay_success'));
            echo 'success';
        } catch (Exception $e) {
            //记录错误日志
            Log::error(json_encode($e->getMessage()).json_encode($_POST), array('alipay_error'));
            echo 'failure';
        }
    }

    //微信回调
    public function wxpayNotify()
    {

        //$postStr = $GLOBALS['HTTP_RAW_POST_DATA'];
        $postStr = file_get_contents("php://input");
        Log::error($postStr, array('wxpay_log'));

        $array_data = json_decode(json_encode(simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

        $attachStr = $array_data['attach'];
        Log::error($attachStr, array('wxpay_log'));
        parse_str($attachStr, $attach);
        $attach['type'] = !isset($attach['type']) ? 2 : $attach['type'];

        $wechatPay = new WechatPay();
        if(!empty($attach['channel']) && !empty($attach['account_name']) && !empty($attach['type'])) {
            $wechatPay->setAccountInfoByDB($attach['channel'], $attach['account_name'], $attach['type']);
        }
        try
        {
            if(empty($postStr)){
                Log::error(__('message.invalid_format'), array('wxpay_error'));
                exit;
            }

            if(!$postData = $wechatPay->getData($postStr)){
                Log::error(__('message.invalid_signature'), array('wxpay_error'));
                exit;
            }

            if ($postData['return_code'] != 'SUCCESS') {
                Log::error(__('message.invalid_return_code'), array('wxpay_error'));
                exit;
            }

            if ($postData['result_code'] != 'SUCCESS') {
                Log::error($postData['err_code_des'], array($postData['err_code']));
                exit;
            }

            $order_id = $postData['out_trade_no'];

            $order = Transaction::query()->where('order_id', '=', $order_id)->first();
            if(empty($order)){
                Log::error(__('message.order_no_not_exist').$order_id, array('wxpay_error'));
                exit;
            }elseif($order->order_id != $attach['order_id']){
                Log::error(__('message.invalid_order_no').$order_id, array('wxpay_error'));
                exit;
            }elseif($order->amount*100 != $postData['cash_fee']){
                Log::error(__('message.invalid_amount').$order_id, array('wxpay_error'));
                exit;
            }

            //$caclDeductionData =$transaction->caclDeduction($order['user_id']);
            $order->trade_no = $postData['transaction_id'];
            $order->pay_status = 1;
            $order->pay_account = $attach['account_name'];
            $order->trans_datetime = date('Y-m-d H:i:s');

            try {
                $order->save();
                Log::error(__('message.success_update'), array('transaction_success_update'));
            } catch (QueryException  $ex) {
                Log::error($ex->getMessage());
            }
            //if(!$transaction->update(array('trans_id'=>$order['trans_id']), array('trade_no'=>$postData['transaction_id'], 'pay_status'=>'1', 'pay_account'=>$attach['account_name'], 'trans_time'=>time(),'flag'=>$caclDeductionData['flag'], 'source'=>$caclDeductionData['source']))){
               // throw new Exception('订单状态改变失败');
            //}

            //if($order['trans_type']==1){
                //$msg='恭喜充值vip成功！';
            //}else{
               // $msg='恭喜您，充值已成功，请点击其他页面进行刷新，可查看最新豆豆余额';
            //}

            //充值推送
            //通过小秘书给被关注着发送消息
            //model("chat")->sendSECMsg($order['user_id'],$msg);

            Log::error(json_encode($postData), array('wxpay_success'));

            echo $wechatPay->returnxml(array('return_code'=>'SUCCESS'));
        } catch (Exception $e) {
            //记录错误日志
            Log::error($e->getMessage(), array('wxpay_error'));
            echo $wechatPay->returnxml(array('return_code'=>'FAIL', 'return_msg'=> $e->getMessage()));
        }
    }

    public function checkTransaction(Request $request)
    {
        $status = 200;
        $message = __('message.success');
        $pay_status = 'unpaid';

        //input fields
        $unique_id = isset($request->uniqueId)?$request->uniqueId:'';
        $oder_id = isset($request->orderId)?$request->orderId:'';

        //get user ID
        $data = User::query()->where('unique_id', '=', $unique_id)->first();
        if(empty($data)) {
            $status = 400;
            $message = __('message.not_register_yet');
        }else {
            $userId = $data->id;

            //search transaction
            $transaction_data = Transaction::query()->where('order_id', '=', $oder_id)
                ->where('user_id', '=', $userId)->first();

            if(empty($transaction_data)){
                $status = 400;
                $message = __('message.transaction_not_found');
            }else{
                if($transaction_data->pay_status){
                    $pay_status = 'paid';
                }
            }
        }

        \AppHelper::instance()->output(array('status' => $status, 'msg' => $message, 'pay_status' => $pay_status));
    }

    public function debug($message = ''){
        \AppHelper::instance()->output(array('status' => 400, 'msg' => $message));
        exit;
    }

}

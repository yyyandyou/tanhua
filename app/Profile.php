<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //use Notifiable;
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   /* protected $hidden = [
        'updated_at', 'updated_datetime',
    ];*/

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    /*protected $casts = [
        'created_datetime' => 'datetime',
    ];*/
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    //use Notifiable;
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'value'
    ];
}

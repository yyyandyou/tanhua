<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'required' => ':attribute是必填项',
    'success_register' => '注册成功',
    'register_fail' => '注册失败',
    'not_register_yet' => '不存在此用户',
    'already_register' => '此用户已注册',
    'success_update' => '资料编辑成功',
    'update_fail' => '资料编辑失败',
    'image_upload_error' => '上传图片错误',
    'user_not_exist' => '不存在此用户',
    'success_login' => '登入成功',
    'invalid_login' => '手机号或密码错误',
    'success' => '成功',
    'invalid_access' => '无效的访问',
    'failed_save_match' => '储存配对资料失败',
    'profile_response_before' => '已配对',
    'profile_not_match_before' => '你与对方并没配对，无法聊天',
    'chat_ended' => '聊天完毕',
    'profile_bookmark_before' => '你已关注对方',
    'invalid_package' => 'VIP特权错误',
    'invalid_pay_mode' => '支付方式错误',
    'invalid_payment_account' => '调用支付失败，没用配置支付账号',
    'create_order_fail' => '创建订单失败,请重试',
    'invalid_phone_number' => '请输入正确手机号',
    'invalid_format' => '请求数据格式不正确',
    'invalid_signature' => '签名验证失败',
    'invalid_return_code' => '通信失败',
    'order_no_not_exist' => '不存在此订单',
    'invalid_order_no' => '预留信息比对错误',
    'invalid_amount' => '订单金额不对',
    'rsa_error' => '验参失败',
    'invalid_app_id' => 'AppID验证失败',
    'invalid_alipay_account' => '支付账号验证失败',
    'transaction_closed' => '交易关闭',
    'transaction_not_found' => '不存在此交易',
    'safe_payment' => '安全支付'

];

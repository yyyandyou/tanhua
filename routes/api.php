<?php

use Illuminate\Http\Request;
use App\Config;

define('PROJECT_PATH', dirname((dirname(__FILE__))));
define('URL_PATH', url('/'));
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

//Route::post('articles', function(Request $request) {
//    return Article::create($request->all);
//});
//Route::get('articles/{id}', 'ArticleController@show');
//Route::post('articles', 'ArticleController@store');
//Route::put('articles/{id}', 'ArticleController@update');
//Route::delete('articles/{id}', 'ArticleController@delete');

// User API
Route::post('user_register', 'UserController@register');
Route::post('user_update', 'UserController@update');
Route::post('user_update_zodiac', 'UserController@updateZodiac');
//Route::post('user_update_silent', 'UserController@updateSilent');
Route::post('user_update_password', 'UserController@updatePassword');
Route::post('user_info', 'UserController@info');
Route::post('user_login', 'UserController@login');
Route::post('user_generate_uniqueID', 'UserController@generateUniqueID');
Route::get('user_debug', 'UserController@debug');
Route::post('user_bookmark', 'UserController@myBookmark');

//Bookmark
Route::post('bookmark_profile', 'UserController@bookmarkProfile');

// VIP Package API
Route::post('package_info', 'VipPackageController@info');

// Profile API
Route::post('profiles_get', 'ProfileController@getProfiles');
Route::post('profile_get_single', 'ProfileController@getSingleProfile');
Route::post('profile_response', 'ProfileController@responseProfile');

// Chat API
Route::post('chat', 'ChatController@chat');
Route::post('get_chat_list', 'ChatController@getChatList');
Route::post('get_random_video_call', 'ChatController@getRandomCall');

// Payment API
Route::post('get_all_pay_modes', 'PaymentController@getAllPaymentModes');
Route::post('pay', 'PaymentController@commit');
//Route::post('wechatPay_notify', 'PaymentController@wxpayNotify');
//Route::post('aliPay_notify', 'PaymentController@alipayNotify');
Route::post('jinfu_notify', 'PaymentController@jinfuNotify');
Route::get('jinfu_notify', 'PaymentController@jinfuNotify');
Route::post('check_order', 'PaymentController@checkTransaction');

// Check redirect Webview
Route::post('check_use_webview', function (Request $request) {
    $url = 'http://app.yousheng666.cn';
    $webview = 0;

    if($request->device == 'android'){
        //check webview
        $config_data = Config::query()->where('name' , '=', 'check_use_webview_android')->first();
        if($config_data->value == '1'){
            $webview = 1;
        }
    }elseif($request->device == 'ios'){
        $config_data = Config::query()->where('name' , '=', 'check_use_webview_ios')->first();
        if($config_data->value == '1'){
            $webview = 1;
        }
    }
    return json_encode(array('webview' => $webview, 'url' => $url));
});

// Banner API
Route::post('get_banners', function (Request $request) {
    $image_root = \AppHelper::instance()->getImageRoot();
    $banners = array($image_root.'/images/banners/coca-cola.jpg');

    return json_encode(array('banners' => $banners));
});
